/*
Copyright © 2023 Dan Jones <danjones@goodevilgenius.org>
*/
package main

import "codeberg.org/danjones000/strip-beats/cmd"

func main() {
	cmd.Execute()
}

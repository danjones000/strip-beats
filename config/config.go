package config

import (
	"errors"
	"os"
	"path/filepath"

	"github.com/BurntSushi/toml"
	"github.com/adrg/xdg"
	"github.com/kirsle/configdir"
)

const (
	AppName   string = "strip-beats"
	Version   string = "0.1.1"
	Url       string = "https://codeberg.org/danjones000/strip-beats"
	Email     string = "danjones@goodevilgenius.org"
	UserAgent string = AppName + "/" + Version + " (" + Url + "; " + Email + ")"
)

type Config struct {
	Source           string            `toml:"source"`
	SourceExtensions []string          `toml:"source_ext"`
	Recurse          bool              `toml:"recurse"`
	SavePath         string            `toml:"save_path"`
	MusicPath        string            `toml:"music_path"`
	Codec            string            `toml:"codec"`
	FfEncoder        string            `toml:"ff_encoder"`
	AllowedCodecs    []string          `toml:"allowed_codec"`
	CodecExt         map[string]string `toml:"codec_ext"`
	AcousticIdKey    string            `toml:"acoustic_id_key"`
	AcousticUserKey  string            `toml:"acoustic_user_key"`
}

var config Config

func newConfig() Config {
	return Config{
		Source:           xdg.UserDirs.Download,
		SourceExtensions: []string{"webm", "mp4"},
		Recurse:          true,
		SavePath:         filepath.Join(xdg.UserDirs.Documents, "StripBeats"),
		MusicPath:        xdg.UserDirs.Music,
		Codec:            "opus",
		FfEncoder:        "libopus",
		AllowedCodecs:    []string{"aac", "opus"},
		CodecExt:         map[string]string{"aac": "m4a", "opus": "opus"},
	}
}

func init() {
	config = newConfig()
	configPath := configdir.LocalConfig("strip-beats")
	if err := configdir.MakePath(configPath); err != nil {
		panic(err)
	}

	configFile := filepath.Join(configPath, "profile.toml")
	if !exists(configFile) {
		file, err := os.Create(configFile)
		if err != nil {
			panic(err)
		}
		err = toml.NewEncoder(file).Encode(config)
		if err != nil {
			panic(err)
		}
		file.Close()
	} else {
		if _, err := toml.DecodeFile(configFile, &config); err != nil {
			panic(err)
		}
	}
}

func GetConfig() Config {
	return config
}

func exists(path string) bool {
	_, err := os.Stat(path)
	return !errors.Is(err, os.ErrNotExist)
}

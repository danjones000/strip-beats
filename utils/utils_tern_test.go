package utils

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTern(t *testing.T) {
	assert.Equal(t, Tern(true, 5, 2), 5)
	assert.Equal(t, Tern(false, 5, 2), 2)
}

func TestTernCall(t *testing.T) {
	five := func() int {
		return 5
	}
	two := func() int {
		return 2
	}
	assert.Equal(t, TernCall(true, five, two), 5)
	assert.Equal(t, TernCall(false, five, two), 2)
}

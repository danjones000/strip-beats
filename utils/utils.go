package utils

import (
	"errors"
	"fmt"
	p "path"
	"strings"
	t "time"
)

func GetShortPath(path string) string {
	base := p.Base(path)
	dir := p.Dir(path)
	dir = p.Base(dir)
	return fmt.Sprintf("%s/%s", dir, base)
}

func HourMinSecToSeconds(time string) (float64, error) {
	parts := strings.Split(time, ":")
	if len(parts) > 3 {
		return 0, errors.New(fmt.Sprintf("Can't parse %s. Must be in HH:MM:SS format", time))
	}
	units := []string{"h", "m", "s"}
	units = units[len(units)-len(parts):]
	f := ""
	for idx, part := range parts {
		f = f + part + units[idx]
	}
	dur, _ := t.ParseDuration(f)
	return dur.Seconds(), nil
}

func Tern[V any](choice bool, one, two V) V {
	if choice {
		return one
	}
	return two
}

func TernCall[V any](choice bool, one, two func() V) V {
	if choice {
		return one()
	}
	return two()
}

package utils

import (
	"errors"
	"fmt"
	"testing"
)

func hmsTest(t *testing.T, time string, expected float64, expecterr error) {
	found, err := HourMinSecToSeconds(time)
	var expecterrmsg, errmsg string
	if err != nil {
		errmsg = err.Error()
	}
	if expecterr != nil {
		expecterrmsg = expecterr.Error()
	}

	if found != expected || errmsg != expecterrmsg {
		t.Fatalf(`HourMinSecToSeconds("%s") = %v, %v, want %v, %v`, time, found, err, expected, expecterr)
	}
}

func TestTwoManyParts(t *testing.T) {
	val := "2:3:4:5:6"
	err := fmt.Sprintf("Can't parse %s. Must be in HH:MM:SS format", val)
	hmsTest(t, val, 0, errors.New(err))
}

func TestJustSeconds(t *testing.T) {
	hmsTest(t, "4.23", 4.23, nil)
}

func TestMinuteSeconds(t *testing.T) {
	hmsTest(t, "2:3.45", 123.45, nil)
}

func TestHourMinuteSeconds(t *testing.T) {
	hmsTest(t, "3:5:12.43", 11112.43, nil)
}

func TestLeadingZeros(t *testing.T) {
	hmsTest(t, "03:05:02.43", 11102.43, nil)
}

func TestWholeSecond(t *testing.T) {
	hmsTest(t, "4:2", 242, nil)
}

# strip-beats

Imagine you have a folder full of music videos. You want to go through those videos, turn it into an audio file, tag them, and add it to your music collection.

This app aims to streamline that.

## Pre-requisites

- ffmpeg
- fpcalc (for fingerprinting audio so that we can identify it and automatically pull tags)
- mpv (for watching the video to ensure you're stipping out non-music elements, like an intro or outro)
- An account on acousticid.org (for querying the fingerprint to get data)

## Install

- Pull the repo
- `go install .`

Hopefully, I'll have binaries downloadable soon.

## Disclaimer

This does not work yet! Some features are implemented, but most are not yet. Have a look around at the code, but don't actually use this at all.

module codeberg.org/danjones000/strip-beats

go 1.20

require (
	dario.cat/mergo v1.0.0
	github.com/BurntSushi/toml v1.3.2
	github.com/adrg/xdg v0.4.0
	github.com/akrennmair/slice v0.0.0-20220105203817-49445747ab81
	github.com/creack/pty v1.1.18
	github.com/google/uuid v1.1.1
	github.com/kirsle/configdir v0.0.0-20170128060238-e45d2f54772f
	github.com/rivo/tview v0.0.0-20230826224341-9754ab44dc1c
	github.com/rkoesters/xdg v0.0.1
	github.com/spf13/cobra v1.7.0
	github.com/stretchr/testify v1.7.0
	github.com/u2takey/ffmpeg-go v0.5.0
	golang.org/x/term v0.11.0
)

require (
	github.com/aws/aws-sdk-go v1.38.20 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/gdamore/tcell/v2 v2.6.0 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/rivo/uniseg v0.4.3 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/u2takey/go-utils v0.3.1 // indirect
	golang.org/x/sys v0.11.0 // indirect
	golang.org/x/text v0.7.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

package tags

import (
	"github.com/google/uuid"
)

type Tags struct {
	Title                     string
	Artist                    string
	AlbumArtist               string `json:"album_artist"`
	Album                     string
	Date                      string
	Comment                   string
	Description               string
	Synopsis                  string
	Url                       string    `json:"purl"`
	AcoustidId                uuid.UUID `json:"ACOUSTID_ID"`
	MusicbrainzReleaseGroupId uuid.UUID `json:"MUSICBRAINZ_RELEASEGROUPID"`
	MusicbrainzAlbumId        uuid.UUID `json:"MUSICBRAINZ_ALBUMID"`
	MusicbrainzAlbumArtistId  uuid.UUID `json:"MUSICBRAINZ_ALBUMARTISTID"`
	MusicbrainzRecordingId    uuid.UUID
	MusicBrainzLabelId        uuid.UUID
	ArtistSort                string
	AlbumArtistSort           string
	ReleaseCountry            string
	Label                     string
	Composer                  string
	Genre                     string
	Disc                      int `json:"-"`
	DiscCount                 int
	Track                     int `json:"-"`
	TrackCount                int
	TrackStr                  string `json:"track"`
	DiscStr                   string `json:"disc"`
}

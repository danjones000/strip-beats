package app

import (
	"fmt"
	"os"

	"codeberg.org/danjones000/strip-beats/files"
	"codeberg.org/danjones000/strip-beats/io/boolean"
	"codeberg.org/danjones000/strip-beats/media"
	"codeberg.org/danjones000/strip-beats/utils"
	"github.com/rkoesters/xdg/trash"
)

var file *media.Probe
var tmpfile *media.Probe

func PickNewFile() media.Probe {
	path := files.PickRandomFile()
	return SetFile(path)
}

func SetFile(path string) media.Probe {
	resetTags()
	f := media.ProbeFile(path)
	file = &f
	copyTagsFromFile()
	return f
}

func SetTmpFile(path string) media.Probe {
	f := media.ProbeFile(path)
	tmpfile = &f
	return f
}

func PickFileWithConf() media.Probe {
	var path string
	var msg string
	good := false
	for !good {
		path = files.PickRandomFile()
		msg = fmt.Sprintf("We've selected %s\nIs that ok?", utils.GetShortPath(path))
		good = boolean.Choose(msg)
		if !good {
			good = boolean.Choose("Would you like to quit?")
			if good {
				return media.Probe{}
			}
		}
	}

	return SetFile(path)
}

func GetFile() media.Probe {
	if file == nil {
		return PickNewFile()
	}
	return *file
}

func GetTmpFile() media.Probe {
	if tmpfile == nil {
		return GetFile()
	}
	return *tmpfile
}

func PickAgain() {
	if tmpfile != nil {
		os.Remove(tmpfile.Format.Path)
	}
	file = nil
	tmpfile = nil
	resetTags()
}

func Finish() {
	if file != nil {
		err := trash.Trash(file.Format.Path)
		if err != nil {
			panic(err)
		}
	}
	PickAgain()
}

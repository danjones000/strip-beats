package app

import (
	"fmt"
	"os"

	"codeberg.org/danjones000/strip-beats/io/boolean"
	"codeberg.org/danjones000/strip-beats/media"
	"codeberg.org/danjones000/strip-beats/media/brainz"
)

type AppStep int

const (
	Pick AppStep = iota
	Watch
	Fade
	Print
	Convert
	Restart
	Quit
)

func quit() {
	PickAgain()
	fmt.Println("Goodbye!")
	os.Exit(0)
}

func testFp() {
	fp, err := media.Fingerprint("/home/drj/MyFiles/Videos/WebShows/YouTube/Dolly_Parton_-_Topic/Just_Because_I_m_a_Woman.Dolly_Parton_-_Topic.Fmv-XQerVkM.webm")
	if err != nil {
		panic(err)
	}
	// fmt.Printf("%+v\n", fp)
	ids, err := media.LookupFingerprint(fp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%+v\n", ids)
	quit()
}

func testMb() {
	id := "497f2f22-809b-4c9e-a692-72f7d8dcaaa2"
	mb, err := brainz.GetRecording(id)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%+v\n", mb)
	quit()
}

func testPrint() {
	// SetFile("/home/drj/MyFiles/Videos/WebShows/YouTube/Dolly_Parton_-_Topic/Just_Because_I_m_a_Woman.Dolly_Parton_-_Topic.Fmv-XQerVkM.webm")
	// SetFile("/home/drj/MyFiles/Videos/WebShows/YouTube/Whitney_Houston/I_Will_Always_Love_You_Ultimate_Collection_Edit.Whitney_Houston.rB7z_l8mBxw.mp4")
	SetFile("/home/drj/MyFiles/Music/Original_Broadway_Cast_of_Hamilton/Hamilton/d02t12-We_Know.m4a")
	// SetFile("/home/drj/MyFiles/Videos/WebShows/YouTube/KaceyMusgravesVEVO/Kacey_Musgraves_-_Biscuits-nGIUtLO_x8g.mp4")
	// SetFile("/home/drj/MyFiles/Videos/WebShows/YouTube/Willie_Nelson_-_Topic/Too_Sick_To_Pray.Willie_Nelson_-_Topic.8QgBXo41j2E.webm")
	print()

	quit()
}

func Run(step AppStep) {
	for step < Quit {
		switch step {
		case Pick:
			PickAgain()
			PickFileWithConf()
			if file == nil {
				quit()
			}
			watch := boolean.Choose(fmt.Sprintf("Would you like to watch %s?", file.ShortPath()))
			if watch {
				step = Watch
			} else {
				step = mainMenu()
			}
		case Watch:
			media.Watch(file.Format.Path)
			fadeAns := boolean.Choose(fmt.Sprintf("Would you like to trim/fade %s?", file.ShortPath()))
			if fadeAns {
				step = Fade
			} else {
				step = mainMenu()
			}
		case Fade:
			fadeFile()
			listenFaded := boolean.Choose(fmt.Sprintf("Would you like to listen to %s?", tmpfile.ShortPath()))
			if listenFaded {
				media.Watch(tmpfile.Format.Path)
			}
			step = mainMenu() // @todo ask to check new file
		case Print:
			print()
			step = mainMenu()
		case Convert:
			convert()
			step = mainMenu()
		case Quit:
			quit()
		default:
			step = mainMenu()
		}
	}
}

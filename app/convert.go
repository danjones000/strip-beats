package app

import (
	"fmt"

	"codeberg.org/danjones000/strip-beats/media"
	"codeberg.org/danjones000/strip-beats/utils"
)

func convert() {
	in := utils.Tern(tmpfile == nil, file, tmpfile)
	out, _ := media.ConvertAndTag(*in, tags)
	fmt.Println(out)

	quit()
}

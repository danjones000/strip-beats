package app

import (
	"fmt"

	"codeberg.org/danjones000/strip-beats/io/list"
)

func (st AppStep) Title() string {
	mustpick := "You need to pick a file"
	switch st {
	case Pick:
		return "Pick a new file"
	case Watch:
		if file == nil {
			return mustpick
		}
		return fmt.Sprintf("Watch %s", file.ShortPath())
	case Fade:
		if file == nil {
			return mustpick
		}
		return fmt.Sprintf("Trim and/or add fade to %s", file.ShortPath())
	case Print:
		if file == nil {
			return mustpick
		}
		return fmt.Sprintf("Should we try to identify %s", file.ShortPath())
	case Convert:
		if file == nil {
			return mustpick
		}
		return fmt.Sprintf("Convert %s?", file.ShortPath())
	case Restart:
		return "Forget current selection"
	case Quit:
		return "Quit"
	default:
		return ""
	}
}

func (st AppStep) Text() string {
	if st == Print {
		return "Use fpcalc, AcousticId, and MusicBrainz to identify the song"
	}
	return ""
}

func (st AppStep) Rune() rune {
	switch st {
	case Pick:
		return 'p'
	case Watch:
		return 'w'
	case Fade:
		return 'f'
	case Restart:
		return 'r'
	case Print:
		return 'a'
	case Convert:
		return 'c'
	case Quit:
		return 'q'
	default:
		return '0'
	}
}

func (st AppStep) Selected() func() {
	return nil
}

func mainMenu() AppStep {
	var steps []list.Option
	if file == nil {
		steps = []list.Option{Pick, Quit}
	} else {
		steps = []list.Option{Pick, Watch, Fade, Print, Convert, Quit}
	}

	step := list.List("What would you like to do next?", steps, nil)

	return step.(AppStep)
}

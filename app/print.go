package app

import (
	"fmt"

	"codeberg.org/danjones000/strip-beats/io/boolean"
	"codeberg.org/danjones000/strip-beats/io/list"
	m "codeberg.org/danjones000/strip-beats/io/message"
	"codeberg.org/danjones000/strip-beats/media"
	"codeberg.org/danjones000/strip-beats/media/brainz"
	"codeberg.org/danjones000/strip-beats/utils"
	"github.com/akrennmair/slice"
)

type recOpt struct {
	rec   brainz.Recording
	r     rune
	score float64
}

func (o recOpt) Title() string {
	return o.rec.Title
}

func (o recOpt) Text() string {
	return fmt.Sprintf(
		"(Score %.2f) By %s - First Released %s - %s %s",
		100*o.score,
		o.rec.FirstArtist().Name,
		o.rec.FirstReleaseDate,
		o.rec.FirstGenre().Name,
		utils.Tern(o.rec.Video, "(Video)", ""))
}

func (o recOpt) Rune() rune {
	return o.r
}

func (o recOpt) Selected() func() {
	return nil
}

func print() {
	if file == nil {
		PickFileWithConf()
	}
	fp, err := media.Fingerprint(file.Format.Path)
	if err != nil {
		panic(err)
	}
	ids, err := media.LookupFingerprint(fp)
	if err != nil {
		panic(err)
	}
	rec, ok := chooseRec(ids)
	if !ok {
		m.Message("Couldn't find a marching recording")
		return
	}

	fmt.Println(rec.Title)

	tags.Title = rec.Title

	tags.MusicbrainzRecordingId = rec.Id
	tags.Date = rec.FirstReleaseDate
	tags.Genre = rec.FirstGenre().Name
	tags.Artist = rec.FirstArtist().Name
	tags.ArtistSort = rec.FirstArtist().SortName
	tags.AcoustidId = rec.AcousticId

	rel := findFirstRelease(rec)
	if !boolean.Choose(fmt.Sprintf("Is %s album (%s %s) correct?", rel.Title, rel.Country, rel.Date)) {
		rel = chooseRel(rec)
	}
	media := rel.Media[0]
	track := media.Tracks[0]
	full, err := brainz.GetReleaseWithMedia(rel.Id)
	if err != nil {
		panic(err)
	}
	albumArtist := full.ArtistCredit[0]

	tags.MusicbrainzReleaseGroupId = full.ReleaseGroup.Id
	tags.MusicbrainzAlbumId = rel.Id
	tags.MusicbrainzAlbumArtistId = albumArtist.Artist.Id
	tags.AlbumArtist = albumArtist.Name
	tags.Album = rel.Title
	if rel.Date != "" {
		tags.Date = rel.Date
	}
	tags.AlbumArtistSort = albumArtist.Artist.SortName
	tags.ReleaseCountry = rel.Country
	if len(full.LabelInfo) > 0 {
		tags.Label = full.LabelInfo[0].Label.Name
		tags.MusicBrainzLabelId = full.LabelInfo[0].Label.Id
	}
	if len(rel.Genres) > 0 && rel.Genres[0].Name != "" {
		tags.Genre = rel.Genres[0].Name
	}
	tags.Disc = media.Position
	tags.DiscCount = len(full.Media)
	tags.Track = track.Position
	tags.TrackCount = media.TrackCount
}

func chooseRec(ids media.IdResults) (brainz.Recording, bool) {
	var recs []list.Option
	var rec brainz.Recording
	var err error
	i := 'a'
	for _, res := range ids.Results {
		for _, rec = range res.Recordings {
			err = brainz.FillRecording(&rec)
			if err != nil {
				panic(err)
			}
			rec.AcousticId = res.Id
			recs = append(recs, recOpt{rec, i, res.Score})
			if rec.Title != "" {
				// Empty titles will be filtered out, so we don't need to increment them
				i = i + 1
			}
		}
	}
	recs = slice.Filter(recs, func(opt list.Option) bool {
		return opt.Title() != ""
	})

	if len(recs) < 1 {
		return rec, false
	}

	return list.List("Which recording is the correct one?", recs, nil).(recOpt).rec, true
}

func findFirstRelease(rec brainz.Recording) brainz.Release {
	var rel brainz.Release
	for _, rel = range rec.Releases {
		if rel.Date == rec.FirstReleaseDate {
			return rel
		}
	}
	if len(rec.Releases) > 0 {
		return rec.Releases[0]
	}
	return brainz.Release{}
}

func chooseRel(rec brainz.Recording) brainz.Release {
	var rels []list.Option
	var rel brainz.Release
	i := 'a'
	for _, rel = range rec.Releases {
		rels = append(rels, relOpt{rel, i})
		if rel.Title != "" {
			// Empty titles will be filtered out, so we don't need to increment them
			i = i + 1
		}
	}
	rels = slice.Filter(rels, func(opt list.Option) bool {
		return opt.Title() != ""
	})

	return list.List("Which releases is the correct one?", rels, nil).(relOpt).rel
}

type relOpt struct {
	rel brainz.Release
	r   rune
}

func (o relOpt) Title() string {
	return o.rel.Title
}

func (o relOpt) Text() string {
	return fmt.Sprintf("%s %s", o.rel.Country, o.rel.Date)
}

func (o relOpt) Rune() rune {
	return o.r
}

func (o relOpt) Selected() func() {
	return nil
}

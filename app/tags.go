package app

import (
	"errors"

	t "codeberg.org/danjones000/strip-beats/media/tags"
)

var tags t.Tags

func resetTags() {
	tags = t.Tags{}
}

func copyTagsFromFile() {
	if file == nil {
		panic(errors.New("Missing file"))
	}

	tags = file.FullTags()
}

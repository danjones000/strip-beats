package app

import (
	"os"
	p "path/filepath"
	s "strings"

	"codeberg.org/danjones000/strip-beats/media"
	"codeberg.org/danjones000/strip-beats/utils"
	"github.com/rivo/tview"
)

func validateNumber(input string, lastChar rune) bool {
	num, err := utils.HourMinSecToSeconds(input)
	if err != nil {
		return false
	}

	if num < 0 {
		return false
	}

	if file == nil {
		return true
	}

	st := file.WantedAudioStream()
	if st == nil {
		return true
	}

	return num <= st.Duration
}

func fadeFile() error {
	base := p.Base(file.Format.Path)
	ext := p.Ext(base)
	base = s.TrimSuffix(base, ext)
	tmp, err := os.CreateTemp("", base+".*.mka")
	if err != nil {
		return err
	}
	tmp.Close()

	var start, stop, up, down float64

	cont := tview.NewApplication()
	form := tview.NewForm().
		AddInputField("Beginning time?", "0", 0, validateNumber, func(input string) {
			start, _ = utils.HourMinSecToSeconds(input)
		}).
		AddInputField("Ending time? (leave 0 for full time)", "0", 0, validateNumber, func(input string) {
			stop, _ = utils.HourMinSecToSeconds(input)
		}).
		AddInputField("Fade in duration?", "0", 0, validateNumber, func(input string) {
			up, _ = utils.HourMinSecToSeconds(input)
		}).
		AddInputField("Fade out duration?", "0", 0, validateNumber, func(input string) {
			down, _ = utils.HourMinSecToSeconds(input)
		}).
		AddButton("Start", func() {
			cont.Stop()
		})

	form.SetBorder(true).SetTitleAlign(tview.AlignLeft)
	if err := cont.SetRoot(form, true).EnableMouse(true).Run(); err != nil {
		return err
	}

	err = media.TrimWithFade(*file, tmp.Name(), start, stop, up, down)
	if err != nil {
		return err
	}
	SetTmpFile(tmp.Name())
	return nil
}

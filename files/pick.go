package files

import "math/rand"

func PickRandomFile() string {
	files := GetCandidates()
	idx := rand.Intn(len(files))

	return files[idx]
}

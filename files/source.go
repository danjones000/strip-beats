package files

import (
	"codeberg.org/danjones000/strip-beats/config"
	"github.com/akrennmair/slice"
	"io/fs"
	"path/filepath"
)

func isValidFile(file string) bool {
	exts := config.GetConfig().SourceExtensions
	return slice.ReduceWithInitialValue(exts, false, func(acc bool, ext string) bool {
		return acc || filepath.Ext(file) == "."+ext
	})
}

func filterToValidFiles(ret []string) []string {
	return slice.Filter(ret, isValidFile)
}

func GetCandidates() []string {
	source := config.GetConfig().Source
	recurse := config.GetConfig().Recurse

	if !recurse {
		ret, _ := filepath.Glob(filepath.Join(source, "*"))
		return filterToValidFiles(ret)
	}

	ret := []string{}
	filepath.WalkDir(source, func(file string, info fs.DirEntry, err error) error {
		if info.IsDir() && err != nil {
			return fs.SkipDir
		}
		if info.IsDir() {
			return nil
		}

		if isValidFile(file) {
			ret = append(ret, file)
		}

		return nil
	})

	return ret
}

package list

import "github.com/rivo/tview"

type Option interface {
	Title() string
	Text() string
	Rune() rune
	Selected() func()
}

type opt struct {
	tit string
	t   string
	r   rune
}

func (o opt) Title() string {
	return o.tit
}

func (o opt) Text() string {
	return o.t
}

func (o opt) Rune() rune {
	return o.r
}

func (o opt) Selected() func() {
	return nil
}

func SimpleItem(title, text string, char rune) Option {
	return opt{title, text, char}
}

func List(title string, items []Option, cb func(*tview.List)) Option {
	if len(items) == 0 {
		return opt{}
	}

	var index int

	app := tview.NewApplication()
	list := tview.NewList()
	for _, item := range items {
		list.AddItem(item.Title(), item.Text(), item.Rune(), item.Selected())
	}
	list.Box.SetBorder(true).SetTitle(title)

	if cb != nil {
		cb(list)
	}

	list.SetSelectedFunc(func(idx int, _ string, _ string, _ rune) {
		index = idx
		app.Stop()
	})

	if err := app.SetRoot(list, true).EnableMouse(true).Run(); err != nil {
		panic(err)
	}

	return items[index]
}

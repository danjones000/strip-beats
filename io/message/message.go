package message

import "github.com/rivo/tview"

func Message(text string) {
	app := tview.NewApplication()
	modal := tview.NewModal()
	if text != "" {
		modal.SetText(text)
	}

	modal.AddButtons([]string{"Ok"}).
		SetDoneFunc(func(buttonIndex int, buttonLabel string) {
			app.Stop()
		})

	if err := app.SetRoot(modal, false).EnableMouse(true).Run(); err != nil {
		panic(err)
	}
}

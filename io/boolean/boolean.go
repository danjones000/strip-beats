package boolean

import "github.com/rivo/tview"

func Choose(text string) bool {
	choice := false
	app := tview.NewApplication()
	modal := tview.NewModal()
	if text != "" {
		modal.SetText(text)
	}

	modal.AddButtons([]string{"Yes", "No"}).
		SetDoneFunc(func(buttonIndex int, buttonLabel string) {
			if buttonLabel == "Yes" {
				choice = true
			}
			app.Stop()
		})

	if err := app.SetRoot(modal, false).EnableMouse(true).Run(); err != nil {
		panic(err)
	}

	return choice
}
